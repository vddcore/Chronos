using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Chronos.Management.DataExchange;
using Chronos.Management.Protocol;
using Chronos.Management.Protocol.Handshake;
using Chronos.Management.Protocol.Handshake.Packets;

namespace Chronos.Management
{
    public class ClientConnection
    {
        private readonly TcpClient _tcpClient;
        
        private NetworkStream _stream;
        private BinaryReaderBE _binaryReader;

        private Dictionary<HandshakeOpCode, PacketHandler> _handshakePacketHandlers;
        
        internal ManagementServer ManagementServer { get; }

        internal ClientConnection(ManagementServer managementServer, TcpClient tcpClient)
        {
            _handshakePacketHandlers = new()
            {
                { HandshakeOpCode.WorldListRequest, new WorldListPacketHandler(this) }
            };
            
            _tcpClient = tcpClient;
            
            ManagementServer = managementServer;
        }
        
        internal async Task Begin()
        {
            _stream = _tcpClient.GetStream();
            _binaryReader = new BinaryReaderBE(_stream, Encoding.UTF8, true);
            
            while (true)
            {
                if (_tcpClient.Available > 0)
                {
                    var opCode = (HandshakeOpCode)_binaryReader.ReadByte();

                    if (!_handshakePacketHandlers.ContainsKey(opCode))
                    {
                        Console.WriteLine($"Unexpected handshake opcode: {opCode}");
                        continue;
                    }
                    
                    _stream.Write(
                        _handshakePacketHandlers[opCode].Process(_binaryReader)
                    );
                }

                await Task.Delay(10);
            }
        }

        internal void Finish(Task beginTask)
        {
            _stream.Dispose();
            _tcpClient.Dispose();
            _binaryReader.Dispose();
        }
    }
}