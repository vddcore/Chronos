namespace Chronos.Management.Protocol.Handshake
{
    internal enum HandshakeOpCode : byte
    {
        LogOut = 35,
        SecretKeyAuthentication = 88,
        WorldListRequest = 255,
        
    }
}