namespace Chronos.Management.Protocol.Handshake
{
    internal enum HandshakeResponse
    {
        OK = 0,
        SecretMismatch = 1,
        UnrecognizedPlayer = 2,
        UnexpectedError = 3
    }
}