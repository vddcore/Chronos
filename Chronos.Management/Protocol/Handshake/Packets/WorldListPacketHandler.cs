using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using Chronos.Management.DataExchange;
using Chronos.Management.DataModel;

namespace Chronos.Management.Protocol.Handshake.Packets
{
    internal class WorldListPacketHandler : PacketHandler
    {
        private List<(int CountryID, string CountryName)> _countryCodes = new();

        private List<WorldInfo> _mockListInfo = new()
        {
            new WorldInfo
            {
                Activity = ".NET 6",
                Address = "localhost",
                Country = CountryFlag.Austria,
                IsMembersOnly = true,
                IsLootShareEnabled = false,
                IsQuickChatOnly = true,
                IsPvP = true,
                Revision = 666,
                WorldID = 0
            },

            new WorldInfo
            {
                Activity = "Management Server",
                Address = "localhost",
                Country = CountryFlag.Australia,
                IsMembersOnly = true,
                IsLootShareEnabled = false,
                IsQuickChatOnly = true,
                IsPvP = true,
                Revision = 420,
                WorldID = 1
            },

            new WorldInfo
            {
                Activity = "is running.",
                Address = "localhost",
                Country = CountryFlag.Belgium,
                IsMembersOnly = false,
                IsLootShareEnabled = true,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 2
            },
            
            new WorldInfo
            {
                Activity = "How's that, Summer?",
                Address = "localhost",
                Country = CountryFlag.Brazil,
                IsMembersOnly = false,
                IsLootShareEnabled = true,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 3
            },

            new WorldInfo
            {
                Activity = "Isn't technology great?",
                Address = "localhost",
                Country = CountryFlag.Canada,
                IsMembersOnly = false,
                IsLootShareEnabled = true,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 4
            },

            new WorldInfo
            {
                Activity = "I like it too.",
                Address = "localhost",
                Country = CountryFlag.Switzerland,
                IsMembersOnly = false,
                IsLootShareEnabled = true,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 10
            },
            
            new WorldInfo
            {
                Activity = "Lorem",
                Address = "localhost",
                Country = CountryFlag.China,
                IsMembersOnly = false,
                IsLootShareEnabled = false,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 11
            },
            
            new WorldInfo
            {
                Activity = "ipsum",
                Address = "localhost",
                Country = CountryFlag.Denmark,
                IsMembersOnly = false,
                IsLootShareEnabled = false,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 12
            },

            new WorldInfo
            {
                Activity = "dolor",
                Address = "localhost",
                Country = CountryFlag.Finland,
                IsMembersOnly = false,
                IsLootShareEnabled = false,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 13
            },
            
            new WorldInfo
            {
                Activity = "sit",
                Address = "localhost",
                Country = CountryFlag.France,
                IsMembersOnly = false,
                IsLootShareEnabled = false,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 14
            },
            
            new WorldInfo
            {
                Activity = "amet",
                Address = "localhost",
                Country = CountryFlag.UK,
                IsMembersOnly = false,
                IsLootShareEnabled = false,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 15
            },
            
            new WorldInfo
            {
                Activity = "consectetur",
                Address = "localhost",
                Country = CountryFlag.Ireland,
                IsMembersOnly = false,
                IsLootShareEnabled = false,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 16
            },
            
            new WorldInfo
            {
                Activity = "adipisici",
                Address = "localhost",
                Country = CountryFlag.India,
                IsMembersOnly = false,
                IsLootShareEnabled = false,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 17
            },
            
            new WorldInfo
            {
                Activity = "elit",
                Address = "localhost",
                Country = CountryFlag.Mexico,
                IsMembersOnly = false,
                IsLootShareEnabled = false,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 18
            },
            
            new WorldInfo
            {
                Activity = "sed",
                Address = "localhost",
                Country = CountryFlag.Netherlands,
                IsMembersOnly = false,
                IsLootShareEnabled = false,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 19
            },
            
            new WorldInfo
            {
                Activity = "do",
                Address = "localhost",
                Country = CountryFlag.Norway,
                IsMembersOnly = false,
                IsLootShareEnabled = false,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 20
            },
            
            new WorldInfo
            {
                Activity = "eiusmod",
                Address = "localhost",
                Country = CountryFlag.NewZealand,
                IsMembersOnly = false,
                IsLootShareEnabled = false,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 21
            },
            
            new WorldInfo
            {
                Activity = "tempor",
                Address = "localhost",
                Country = CountryFlag.Portugal,
                IsMembersOnly = false,
                IsLootShareEnabled = false,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 22
            },
            
            new WorldInfo
            {
                Activity = "incididunt",
                Address = "localhost",
                Country = CountryFlag.Sweden,
                IsMembersOnly = false,
                IsLootShareEnabled = false,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 23
            },
            
            new WorldInfo
            {
                Activity = "ut.",
                Address = "localhost",
                Country = CountryFlag.USA,
                IsMembersOnly = false,
                IsLootShareEnabled = false,
                IsQuickChatOnly = false,
                IsPvP = true,
                Revision = 69,
                WorldID = 24
            },
        };

        internal int LastWorldUpdateTimeStamp { get; private set; } =
            (int)((DateTimeOffset)DateTime.Now).ToUnixTimeMilliseconds();

        public WorldListPacketHandler(ClientConnection client)
            : base(client)
        {
            var values = typeof(CountryFlag).GetEnumValues();

            foreach (CountryFlag value in values)
            {
                var info = typeof(CountryFlag).GetMember(value.ToString()).First();
                var attrib = info.GetCustomAttribute<DescriptionAttribute>();

                if (attrib != null)
                {
                    _countryCodes.Add((
                        (int)value,
                        attrib.Description
                    ));
                }
                else
                {
                    _countryCodes.Add((
                        (int)value,
                        value.ToString()
                    ));
                }
            }
        }

        internal override byte[] Process(BinaryReaderBE br)
        {
            var updateTimeStamp = br.ReadInt32();

            using (var ms = new MemoryStream())
            {
                using (var bw = new BinaryWriterBE(ms))
                {
                    // What is this?
                    bw.Write((sbyte)0);
                    
                    // (size of payload) - (size of header)
                    bw.Write((short)0);
                    
                    // Whether to send 1 update per 5 seconds.
                    // Or to let client send 5 queries without any timeouts.
                    bw.Write(Client.ManagementServer.Config.ThrottleQueries);

                    if (LastWorldUpdateTimeStamp == updateTimeStamp)
                    {
                        bw.Write(false);
                    }
                    else
                    {
                        bw.Write(true);
                        WriteWorldListInfo(bw);
                    }
                    WritePlayerInfo(bw);

                    ms.Seek(1, SeekOrigin.Begin);
                    bw.Write((short)(ms.Length - 3));
                }

                LastWorldUpdateTimeStamp = updateTimeStamp;
                return ms.ToArray();
            }
        }

        private static int _lalala = 0;

        private void WriteWorldListInfo(BinaryWriterBE bw)
        {
            WriteCountryInfo(bw);

            // How much to offset world ID display by.
            // e.g. info.WorldID + X
            //
            // It must be within the WorldLimit range or the client will crash.
            //
            bw.WritePacked(Client.ManagementServer.Config.WorldIdOffset);

            // How many worlds do we support at the same time?
            bw.WritePacked(Client.ManagementServer.Config.WorldLimit);

            // How many worlds do we have online?
            bw.WritePacked(_mockListInfo.Count);

            foreach (var info in _mockListInfo)
            {
                bw.WritePacked(info.WorldID);

                bw.WritePacked(
                    (sbyte)_countryCodes.IndexOf(
                        _countryCodes.First(x => x.CountryID == (int)info.Country)
                    )
                );

                bw.Write(info.SettingsBitField);
                bw.WriteJagString(info.Activity);
                bw.WriteJagString(info.Address);
            }

            bw.Write(LastWorldUpdateTimeStamp);
        }

        private void WriteCountryInfo(BinaryWriterBE bw)
        {
            bw.WritePacked(_countryCodes.Count);

            for (var i = 0; i < _countryCodes.Count; i++)
            {
                if (_countryCodes[i].CountryID == -1)
                {
                    Console.WriteLine(_lalala);
                    bw.WritePacked(_lalala++);
                }
                else
                {
                    bw.WritePacked(_countryCodes[i].CountryID);
                }
                
                bw.WriteJagString(_countryCodes[i].CountryName);
            }
        }

        private void WritePlayerInfo(BinaryWriterBE bw)
        {
            foreach (var info in _mockListInfo)
            {
                bw.WritePacked(info.WorldID);
                bw.Write((short)420);
            }
        }
    }
}