using Chronos.Management.DataExchange;

namespace Chronos.Management.Protocol
{
    internal abstract class PacketHandler
    {
        protected ClientConnection Client { get; }

        internal PacketHandler(ClientConnection client)
        {
            Client = client;
        }
        
        internal abstract byte[] Process(BinaryReaderBE br);
    }
}