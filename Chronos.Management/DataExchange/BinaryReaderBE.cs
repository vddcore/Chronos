using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace Chronos.Management.DataExchange
{
    public class BinaryReaderBE : BinaryReader
    {
        public BinaryReaderBE(string fileName)
            : base(File.OpenRead(fileName))
        {
        }

        public BinaryReaderBE(Stream input)
            : base(input)
        {
        }

        public BinaryReaderBE(Stream input, Encoding encoding)
            : base(input, encoding)
        {
        }

        public BinaryReaderBE(Stream input, Encoding encoding, bool leaveOpen)
            : base(input, encoding, leaveOpen)
        {
        }

        public override short ReadInt16()
            => SwapEndianness(BitConverter.ToInt16);

        public override ushort ReadUInt16()
            => SwapEndianness(BitConverter.ToUInt16);

        public override int ReadInt32()
            => SwapEndianness(BitConverter.ToInt32);

        public override uint ReadUInt32()
            => SwapEndianness(BitConverter.ToUInt32);

        public override long ReadInt64()
            => SwapEndianness(BitConverter.ToInt64);

        public override ulong ReadUInt64()
            => SwapEndianness(BitConverter.ToUInt64);

        public override double ReadDouble()
            => SwapEndianness(BitConverter.ToDouble);

        public override float ReadSingle()
            => SwapEndianness(BitConverter.ToSingle);

        public override Half ReadHalf()
            => SwapEndianness(BitConverter.ToHalf);

        public override int Read(byte[] buffer, int index, int count)
        {
#if DEBUG
            Console.Write("<- ");

            foreach (var b in buffer)
            {
                Console.Write($"{b:X2} ");
            }

            Console.WriteLine();
#endif
            
            return base.Read(buffer, index, count);
        }

        public virtual UInt24 ReadUInt24()
        {
            var le = ReadBytes(3);
            Array.Reverse(le);

            return new UInt24(le);
        }

        private T SwapEndianness<T>(Func<byte[], int, T> convFunc) where T : struct
        {
            var be = base.ReadBytes(Marshal.SizeOf<T>());
            Array.Reverse(be);

            return convFunc(be, 0);
        }
    }
}