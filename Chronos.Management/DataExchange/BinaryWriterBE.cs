using System;
using System.IO;
using System.Text;

namespace Chronos.Management.DataExchange
{
    public class BinaryWriterBE : BinaryWriter
    {
        public BinaryWriterBE(string fileName)
            : base(File.OpenRead(fileName))
        {
        }
        
        public BinaryWriterBE(Stream output) 
            : base(output)
        {
        }

        public BinaryWriterBE(Stream output, Encoding encoding) 
            : base(output, encoding)
        {
        }

        public BinaryWriterBE(Stream output, Encoding encoding, bool leaveOpen) 
            : base(output, encoding, leaveOpen)
        {
        }

        public void WritePacked(long value)
        {
            if (value <= sbyte.MaxValue)
            {
                Write((sbyte)value);
            }
            else if (value <= short.MaxValue)
            {
                Write((short)(value + 32768));
            }
            else if (value <= int.MaxValue)
            {
                Write((int)(value + 32768));
            }
            else
            {
                Write(value);
            }
        }

        public void WriteJagString(string value)
        {
            var bytes = Encoding.UTF8.GetBytes(value);
            Write((sbyte)0);
            Write(bytes);
            Write((sbyte)0);
        }
        
        public override void Write(string value)
        {
            var bytes = Encoding.UTF8.GetBytes(value);
            Write(bytes);
            Write((sbyte)0);
        }

        public override void Write(short value)
            => WritePrimitive(value);

        public override void Write(ushort value)
            => WritePrimitive(value);

        public override void Write(int value)
            => WritePrimitive(value);

        public override void Write(uint value)
            => WritePrimitive(value);

        public override void Write(long value)
            => WritePrimitive(value);

        public override void Write(ulong value)
            => WritePrimitive(value);

        public override void Write(double value)
            => WritePrimitive(value);

        public override void Write(float value)
            => WritePrimitive(value);

        public override void Write(Half value)
            => WritePrimitive(value);

        public void Write(UInt24 value)
        {
            var le = value.GetBytes();
            Array.Reverse(le);

            Write(le);
        }

        private void WritePrimitive<T>(T value) where T: struct
        {
            var le = (byte[])typeof(BitConverter)
                !.GetMethod("GetBytes", new[] { typeof(T) })
                !.Invoke(null, new object[] { value });
            
            Array.Reverse(le!);
            Write(le);
        }
    }
}