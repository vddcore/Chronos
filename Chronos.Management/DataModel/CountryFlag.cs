using System.ComponentModel;

namespace Chronos.Management.DataModel
{
    public enum CountryFlag
    {
        Austria = 15,
        Australia = 16,
        Belgium = 22,
        Brazil = 31,
        Canada = 38,
        Switzerland = 43,
        China = 48,
        Denmark = 58,
        Finland = 69,
        France = 74,
        
        [Description("United Kingdom")]
        UK = 77,

        Ireland = 101,
        India = 103,
        Mexico = 152,
        Netherlands = 161,
        Norway = 162,
        
        [Description("New Zealand")]
        NewZealand = 166,
        Portugal = 179,
        
        Sweden = 191,
        
        [Description("United States")]
        USA = 225,
        
    }
}