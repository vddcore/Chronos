namespace Chronos.Management.DataModel
{
    public enum Response
    {
        Unexpected,
        CouldNotDisplayAd,
        OK,
        InvalidCredentials,
        AccountDisabled,
        AlreadyOnline,
        Updated,
        WorldFull,
        LoginServerOffline,
        LoginLimitExceeded,
        BadSessionId,
        PasswordToWeak,
        UnauthorizedMembersWorldAccess,
        CouldNotLogin,
        UpdateInProgress,
        TooManyIncorrectLogins = 16,
        FreePlayerInMembersArea,
        AccountLocked,
        ClosedBetaOngoing,
        InvalidLoginServer,
        MovingFromAnotherWorld,
        ProfileLoadingError = 24,
        Banned = 26
    }
}