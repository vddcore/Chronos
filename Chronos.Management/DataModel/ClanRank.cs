namespace Chronos.Management.DataModel
{
    public enum ClanRank
    {
        Everyone = -1,
        Friend,
        Recruit,
        Corporal,
        Sergeant,
        Lieutenant,
        Captain,
        General,
        Owner,
        Administrator = 127
    }
}