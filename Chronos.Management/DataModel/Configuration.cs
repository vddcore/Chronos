using System.Text.Json.Serialization;

namespace Chronos.Management.DataModel
{
    public class Configuration
    {
        public class DatabaseInformation
        {
            [JsonPropertyName("database_name")]
            public string Name { get; set; } = "global";

            [JsonPropertyName("database_username")]
            public string Username { get; set; } = "root";

            [JsonPropertyName("database_password")]
            public string Password { get; set; } = string.Empty;

            [JsonPropertyName("database_host")]
            public string HostAddress { get; set; } = "127.0.0.1";

            [JsonPropertyName("database_port")]
            public ushort Port { get; set; } = 3306;
        }

        [JsonPropertyName("DatabaseInformation")]
        public DatabaseInformation Database { get; set; } = new();

        [JsonPropertyName("world_limit")]
        public int WorldLimit { get; set; } = 1000;
            
        [JsonPropertyName("world_id_offset")]
        public int WorldIdOffset { get; set; } = 1;
            
        [JsonPropertyName("worldhop_delay_ms")]
        public int WorldHopDelayMilliseconds { get; set; } = 20000;

        [JsonPropertyName("throttle_queries")]
        public bool ThrottleQueries { get; set; } = true;

        [JsonPropertyName("secret_key")]
        public string SecretKey { get; set; } = "2009scape_development";

        [JsonPropertyName("port")]
        public ushort Port { get; set; } = 5555;

        [JsonPropertyName("address")]
        public string Address { get; set; } = "127.0.0.1";
    }
}