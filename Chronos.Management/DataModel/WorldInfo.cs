namespace Chronos.Management.DataModel
{
    public class WorldInfo
    {
        public int WorldID { get; set; }
        public string Address { get; set; }
        public int Revision { get; set; }
        public CountryFlag Country { get; set; }
        public string Activity { get; set; }
        public bool IsMembersOnly { get; set; }
        public bool IsPvP { get; set; }
        public bool IsQuickChatOnly { get; set; }
        public bool IsLootShareEnabled { get; set; }

        public int SettingsBitField
        {
            get
            {
                var ret = 0;

                ret |= (IsMembersOnly ? 1 : 0) << 0;
                ret |= (IsPvP ? 1 : 0) << 1;
                ret |= (IsQuickChatOnly ? 1 : 0) << 2;
                ret |= (IsLootShareEnabled ? 1 : 0) << 3;
                
                return ret;
            }
        }
    }
}