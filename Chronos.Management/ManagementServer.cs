using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Chronos.Management.DataModel;

namespace Chronos.Management
{
    public class ManagementServer
    {
        private readonly TcpListener _tcpListener;
        private readonly CancellationTokenSource _cts;

        public Configuration Config { get; }
        public bool IsRunning { get; private set; }

        public ManagementServer(Configuration config)
        {
            Config = config;
            _tcpListener = new TcpListener(IPAddress.Parse(Config.Address), Config.Port);
            _cts = new CancellationTokenSource();
        }

        public async Task Begin()
        {
            if (IsRunning)
            {
                throw new InvalidOperationException("Server is already running.");
            }
            
            IsRunning = true;
            await StartTcpListener();
        }
        
        private async Task StartTcpListener()
        {
            _tcpListener.Start();

            while (IsRunning)
            {
                var conn = new ClientConnection(
                    this,
                    await _tcpListener.AcceptTcpClientAsync(_cts.Token)
                );
                
#pragma warning disable CS4014 We don't wait for connections to end.
                Task.Run(conn.Begin)
                    .ContinueWith(conn.Finish);
#pragma warning restore CS4014
            }
        }
    }
}