﻿using System;
using System.Threading.Tasks;
using Chronos.Management;
using Chronos.Management.DataModel;

namespace Chronos
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Yeet!");
            
            var ms = new ManagementServer(new Configuration());
            await ms.Begin();
        }
    }
}